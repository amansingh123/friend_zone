from django.contrib import admin
from finalapp.models import register, Message, Post,followUsers,inbox, postcomment,likes
# Register your models here.
class registerAdmin(admin.ModelAdmin):
    list_display =['id','user','user_id']

class followUsersAdmin(admin.ModelAdmin):
    list_display =['id','follower','following','is_friend']
    search_fields =['follower']

admin.site.register(register,registerAdmin)
admin.site.register(Message)
admin.site.register(Post)
admin.site.register(followUsers,followUsersAdmin)
admin.site.register(inbox)
admin.site.register(postcomment)
admin.site.register(likes)
