from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import registerForm,registerForm2
from django.contrib.auth import login, authenticate
from django.contrib.auth import logout as logout_view
from django.views.generic import TemplateView,ListView, DetailView,CreateView, UpdateView, DeleteView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Post, register,followUsers,inbox, postcomment, likes, Message
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django import forms
from django.core import serializers
# Create your views here.
def index(request):
    form=registerForm()
    form1 = registerForm2()
    if request.method == 'POST':
        if "register" in request.POST:
            form = registerForm(data=request.POST)
            form1 = registerForm2(request.POST,request.FILES)

            if form.is_valid() and form1.is_valid():
                user = form.save()
                user.set_password(user.password)
                user.save()

                frm2 = form1.save(commit=False)
                frm2.user = user

                if "profile_pic" in request.FILES:
                    frm2.profile_pic = request.FILES['profile_pic']
                    print("Success")
                frm2.save()
                register = "register Successfully"
                return render(request,'index.html',{'rf':form,'rf2':form1,'rg':register})

            else:
                print(form1.errors,form.errors)
                register = ("OOPs!! Could Not Register!! Try Again!!!")
                return render(request,'index.html',{'rf':form,'rf2':form1,'rg':register})


        elif "login" in request.POST:
            print("login Button")
            usern = request.POST['un']
            passw = request.POST['pwd']
            user = authenticate(username = usern, password = passw)
            if user:
                print("Hello")
                if user.is_active:
                    login(request,user)
                    return HttpResponseRedirect('/finalapp')
                else:
                    register="You are not active user!!!"
                    return render(request,'index.html',{'rf':form,'rf2':form1,'rg':register})
            else:
                register="Could not login !!! You are not Registered User!!!"
                return render(request,'index.html',{'rf':form,'rf2':form1,'rg':register})
        
        elif "feedbtn" in request.POST:
            name = request.POST['feedname']
            email = request.POST['feedemail']
            msz = request.POST['feedmessage']
            feedback = Message(name = name, email= email, message=msz)
            feedback.save()
            register = "Thanks for your Anonymous Feedback!!!"
            return render(request,'index.html',{'rf':form,'rf2':form1,'rg':register})

    data = User.objects.order_by('-id')[:5]
    for ab in data:
        print(ab)
    return render(request,'index.html',{'rf':form,'rf2':form1,'dt':data})
def registeration(request):
    return render(request,'registration_form.html',{'rf':form})

class dashboard(LoginRequiredMixin,TemplateView):
    template_name="dashboard.html"
    def get_context_data(self,*args,**kwargs):
        loginid = self.request.user.id
        data=super().get_context_data(*args,**kwargs)
        data['u'] = register.objects.exclude(user_id = loginid)
        data['userp'] = register.objects.filter(user_id = loginid)
        friend_ll = []
        for i in data['u']:
            myd = {}
            if followUsers.objects.filter(follower=loginid, following=i.user.id, is_friend=True):
                myd['btn'] = 'Unfollow'
                myd['btn1'] = 'btn btn-danger'
            else:
                myd['btn'] = 'Follow'
                myd['btn1'] = 'btn btn-warning'
                myd['username'] = i.user.username
                myd['profile_pic'] = i.profile_pic
                myd['registered_id'] = i.registered_at
                myd['user_id'] = i.user.id
                friend_ll.append(myd)
        data['p'] = friend_ll
        # print(data['p'])
        success_url = reverse_lazy('finalapp:profile')
        return data
class PostListView(ListView):
    model = Post
    # success_url =reverse_lazy("cbvapp:list")

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['post_list'] = Post.objects.filter(author=self.request.user.id)
        context['userp'] = register.objects.filter(user_id = self.request.user.id)
        print(context)
        return context

class PostDetailView(DetailView):
    model = Post
    context_object_name = "post_detail"
    template_name = 'finalapp/post_detail.html'
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['userp'] = register.objects.filter(user_id = self.request.user.id)
        return context

class PostUpdateView(UpdateView):
    model = Post
    fields=['title','image','text']

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['userp'] = register.objects.filter(user_id = self.request.user.id)
        return context

class PostDeleteView(DeleteView):
    model = Post
    success_url =reverse_lazy('finalapp:list')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['userp'] = register.objects.filter(user_id = self.request.user.id)
        return context

class PostCreateView(LoginRequiredMixin,CreateView):
    context_object_name='create'
    model=Post
    fields=('title','image','text')
    # success_url = reverse_lazy('finalapp:profile')

    def form_valid(self, form):
      form.instance.author = self.request.user
      form.save()
      print(self.request.user)
      return HttpResponseRedirect('/finalapp')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['userp'] = register.objects.filter(user_id = self.request.user.id)
        return context


class UserView(LoginRequiredMixin,CreateView):
    fields = ('date_of_birth','gender','profile_pic','hometown','education_details','interested_in','marital_status','worked_at','about_you')
    model=register
    # template_name = 'menu/order_form.html'
    # def get_object(self):
    #     return register.objects.get(id=self.request.GET.get('pk'))

class registerUpdateView(UpdateView):
    model = register
    fields = ['date_of_birth','gender','hometown','education_details','interested_in','marital_status','worked_at','about_you','profile_pic']
    template_name = 'finalapp/register_form.html'
    success_url = reverse_lazy('finalapp:user_profile')



@login_required
def logout(request):
    logout_view(request)
    return HttpResponseRedirect('/')
def users(request):
    ab = register.objects.all()
    return render(request,'users.html',{'u':ab})

def follow(request):
    loginid = request.user.id
    followid = request.GET.get('id')
    a = [loginid, followid]
    if(followUsers.objects.filter(follower=loginid,following=followid).count()==0):

        data = followUsers(follower=loginid,following=followid )
        data.save()
        data.is_friend=True
        data.save()
        print("Saved Successfully")
    else:
        data = followUsers(follower=loginid,following=followid )
        data.is_friend=False
        data.save()
        followUsers.objects.filter(follower=loginid,following=followid).delete()
        print("Already Exists and Deleted")
    return HttpResponseRedirect('/finalapp/')

def friends(request):
    data = followUsers.objects.filter(follower__contains=request.user.id)
    userp = register.objects.filter(user_id = request.user.id)
    friend_list = []
    dict={}
    for i in data:
        dict={}
        # print("follower",i.follower)
        # print("f",i.following)
        loginid = request.user.id
        uid=i.following
        friends = User.objects.get(pk=uid)
        friends2 = register.objects.get(user_id=uid)
        dict['id']=friends.id
        dict['un']=friends.username
        dict['email']=friends.email
        dict['fn']=friends.first_name
        dict['lnm']=friends.last_name
        dict['lg']=friends.last_login
        if(followUsers.objects.filter(follower=loginid).count()==0):
            dict['btt'] = 'f'
        else:
            dict['btt'] = 'u'
        dict['pp']=friends2.profile_pic
        friend_list.append(dict)
    # print(friend_list)
    total=len(friend_list)
    return render(request,'friends.html',{'data':data,'friendList':friend_list,'total':total,'userp':userp})

    return render(request,'friends.html',{'data':data})
def save_message_ajax(request):
    if request.method == "POST":
        sndr = request.user.id
        rcvr = request.POST.get('rcvr')
        message = request.POST.get('message')
        message_detail = inbox(sender=sndr, receiver=rcvr,message=message)
        message_detail.status=True
        message_detail.save()
        print('saved successfully')
        return HttpResponse("Success")
    ## Function End
def show_message_all(request):
    if request.method == "POST":
        rcvr = request.POST.get('rcvr')
        sndr = request.POST.get('sndr')
        sent_mszs = inbox.objects.filter(sender=sndr,receiver=rcvr)|inbox.objects.filter(sender=rcvr,receiver=sndr).order_by('-created_at')
        serialized_obj = serializers.serialize('json',sent_mszs)
        return HttpResponse(serialized_obj, content_type = 'application/json')


###########To GET SENDER Names
def ajax_fun(request):
    if request.method == "POST":
        if request.POST.get('usernamefromid'):
            uid = request.POST.get('usernamefromid')
            username = User.objects.get(pk=uid)
            return HttpResponse(username)

def user_inbox(request):
    userp = register.objects.filter(user_id = request.user.id)
    sndr = request.user.id
    rcvr=request.GET.get('id')
    a=[sndr,rcvr]
    # if request.method == "POST":
    #     ##### SEND MESSAGE ##########
    #     msz = request.POST.get('message')
    #     message_detail = inbox(sender=sndr, receiver=rcvr,message=msz)
    #     message_detail.status=True
    #     message_detail.save()
    #     print('saved successfully')
    ###### RECIEVE MESSAGE #####
    sent_mszs = inbox.objects.filter(sender=sndr,receiver=rcvr)|inbox.objects.filter(sender=rcvr,receiver=sndr).order_by('-created_at')
    # received = inbox.objects.filter(sender=rcvr,receiver=sndr).order_by('-created_at')
    # mszDict = []
    mszdict = {}
    for i in sent_mszs:
        pass
        # print(i.id)
            ## INDOX OF PARTICULAR USER ##
    rec= register.objects.filter(user_id=rcvr)
    for m in rec:
        mszdict['profile_pic'] = m.profile_pic
        mszdict['name'] = m.user.username
        mszdict['lastlogin'] = m.user.last_login
        mszdict['dob'] = m.date_of_birth
        mszdict['loginuser'] = sndr
        mszdict['sendto'] = rcvr
    return render(request,'inbox.html',{'detail':rec,'det':mszdict,'sent':sent_mszs,'userp':userp})

class message_list(ListView):
    model = inbox
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        d = []
        m = {}
        abcd = inbox.objects.filter(receiver=self.request.user.id).values()
        for i in abcd:
            aaa = User.objects.get(id=i['sender'])
            m['sender'] = aaa.username
            m['receiver'] = i['receiver']
            m['message'] = i['message']
            d.append(m)
        print(d)
        context['post_list'] = d
        return context

def updatePass(request):
    alert=''
    userp = register.objects.filter(user_id = request.user.id)
    if request.method=="POST":
        previous= request.POST['op']
        new1 = request.POST['n1']
        new2 = request.POST['n2']
        if new1 != new2:
            alert = "Password Does Not Match"
        else:
            data = User.objects.get(username__exact=request.user)
            data.set_password(new1)
            data.save()
            alert="Password Changed Successfully"
    return render(request,'change_password.html',{'al':alert,'userp':userp})


def news_feed(request):
    ls=[]
    userp = register.objects.filter(user_id = request.user.id)
    posts = Post.objects.all().order_by('-id')


    for i in posts:
        comments = postcomment.objects.filter(postid=i.id).order_by('-id')
        d=len(comments)
        ls.append(d)

    return render(request,'newsfeed.html',{'userp':userp,'posts':posts,'t':ls})

def feed_description(request):
    userp = register.objects.filter(user_id = request.user.id)
    post_id = request.GET.get('id')
    getpost = Post.objects.get(id=post_id)
    comments = postcomment.objects.filter(postid=post_id).order_by('-id')
    total = len(comments)
    print(comments)
    fk = register.objects.get(user_id=request.user.id)

###LIKES
    like = likes.objects.filter(postid=post_id).order_by('-id')
    totallikes = len(like)
    tl = Post.objects.get(id=post_id)
    tl.likes = totallikes  # change field
    tl.save()



    if request.method == 'POST':
        cm = request.POST['com']
        pcomment = postcomment.objects.create(postid=post_id, commenterid=fk, comment=cm)
        pcomment.save()
        comments = postcomment.objects.filter(postid=post_id).order_by('-id')
        total = len(comments)
        t = Post.objects.get(id=post_id)
        t.comments = total  # change field
        t.save()



        print("saved successfully")


    postdict={}
    postdict['id'] = getpost.id
    postdict['title'] = getpost.title
    postdict['text'] = getpost.text
    postdict['create'] = getpost.create_date
    postdict['img'] = getpost.image
    return render(request,'feed_description.html',{'userp':userp,'postdict':postdict,'comm':comments, 'total':total,'tl':totallikes})



def userProfile(request):
    userfields = User.objects.get(pk=request.user.id)
    additional = register.objects.get(user_id=request.user.id)
    profdict = {}

    profdict['id'] = additional.id
    profdict['username'] = userfields.username
    profdict['email'] = userfields.email
    profdict['first_name'] = userfields.first_name
    profdict['last_name'] = userfields.last_name
    profdict['profile_pic'] = additional.profile_pic
    profdict['gender'] =additional.gender
    profdict['work'] =additional.worked_at
    profdict['interests'] = additional.interested_in
    profdict['status'] = additional.marital_status
    profdict['dob'] = additional.date_of_birth
    profdict['education']=additional.education_details
    profdict['about'] = additional.about_you
    profdict['current'] = additional.current_city
    profdict['home'] = additional.hometown
    profdict['registered'] = additional.registered_at
    print(profdict)
    userp = register.objects.filter(user_id = request.user.id)

    return render(request,'userprofile.html', {'profile':profdict,'userp':userp})

def postlike(request):
    loginid = request.user.id
    postid = request.GET.get('id')
    a = [loginid, postid]
    if(likes.objects.filter(follower=loginid,postid=postid).count()==0):

        data = likes(follower=loginid,postid=postid )
        data.save()
        data.is_like=True
        data.save()
        print("Saved Successfully")
        return HttpResponseRedirect('/finalapp/user/newsfeed')

    else:
        # data = followUsers(follower=loginid,following=followid )
        # data.is_friend=False
        # data.save()
        likes.objects.filter(follower=loginid,postid=postid).delete()
        print("Already Liked and Unlike")
    return HttpResponseRedirect('/finalapp/user/newsfeed')
