from django import forms
from django.contrib.auth.models import User
from .models import register
class registerForm(forms.ModelForm):
    password=forms.CharField(widget=forms.PasswordInput())
    confirm_password=forms.CharField(widget=forms.PasswordInput())
    def clean(self):
        clean_data=super().clean()
        pwd = clean_data['password']
        cpwd= clean_data['confirm_password']
        if pwd != cpwd:
            raise forms.ValidationError("Password Does Not Match")
    class Meta():
        model=User
        fields=('first_name','last_name','username','email','password')

class registerForm2(forms.ModelForm):
    GENDER = [('male','male'),
            ('female','female')]
    date_of_birth= forms.CharField(widget=forms.TextInput(attrs={'type':'date','help_text':'Date Of Birth'}))
    gender = forms.ChoiceField(choices=GENDER,widget=forms.RadioSelect())
    class Meta():
        model = register
        fields = ('date_of_birth','gender','profile_pic')
